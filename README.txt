Sometimes is useful to have a direct way to send messages. This modules does it. Under the hood it just triggers an event in Rules and exposes the fields in the form as targets to be filled by the message module.

Note: it also hides the guidelines and input types below the body of the direct message forms. You can override the simple css to avoid it.


