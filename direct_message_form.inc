<?php

/**
 * @file
 * This is the Form API Tutorial from the handbook.
 *
 * It goes through 10 form examples of increasing complexity to demonstrate
 * Drupal 7 Form API.
 *
 * Links are provided inline for the related handbook pages.
 *
 * @see http://drupal.org/node/262422
 */

function direct_message_form_tutorial() {
  return t('This is a set of 10 form tutorials tied to the <a href="http://drupal.org/node/262422">Drupal handbook</a>.');
}

/**
 * This first form function is from the @link http://drupal.org/node/717722 Form Tutorial handbook page @endlink
 *
 */
function direct_message_form_new($form, &$form_state) {
 $form['to'] = array(
  '#type' => 'textfield',
  '#title' => t('To'),
  '#maxlength' => 60,
  '#autocomplete_path' => 'user/autocomplete/multiple',
  '#default_value' => "",
  '#weight'             => -2,
  '#size' => 20,
  );
  $form['subject'] = array(
    '#type'               => 'textfield',
    '#title'              => t('Subject'),
    '#size'               => 20,
    '#maxlength'          => 255,
    '#default_value'      => "",
    '#weight'             => -1,
  );
  $form['body'] = array(
    '#type'               => 'text_format',
    '#title'              => t('Message'),
    '#rows'               => 6,
//     '#weight'             => 0,
    '#default_value'      => "",
    '#resizable'          => TRUE,
  );
  $form['#attributes']['class'][] = 'direct-message';
  $form['actions']['submit'] = array(
    '#type'               => 'submit',
    '#value'              => t('Send message'),
    '#weight'             => 49,
  );
  
  $titleCancel= t('Cancel');
  $url=".";
  if (isset($_REQUEST['destination'])) {
    $url = $_REQUEST['destination'];
  }
  $form['actions']['cancel'] = array(
    '#markup'              => l($titleCancel, $url, array('attributes' => array('id' => 'edit-cancel'))),
    '#weight'             => 50,
  );
  // If a module (e.g. OG) adds a validate or submit callback to the form in
  // field_attach_form, the form system will not add ours automatically
  // anymore. Therefore, explicitly adding them here.
  $form['#submit'] = array('direct_message_form_new_submit');
  //$form['#validate'] = array('privatemsg_new_validate'); TODO
  return $form;
}

/**
 * Submit callback for the direct_message_form_new form.
 */
function direct_message_form_new_submit($form, &$form_state) {
  $message = (object)$form_state['values'];
  $users=array();
  $usernames=drupal_explode_tags($message->to);
  foreach ($usernames as $username){
    array_push($users,user_load_by_name($username));
  }
   rules_invoke_event_by_args('direct_message_form_new_dm', array('to'=>$users, 'subject'=>$message->subject,'body'=>$message->body['value'])); 
}

