<?
/**
* Implementation of hook_rules_event_info().
* @ingroup rules
*/
function direct_message_form_rules_event_info() {
  return array(
    'direct_message_form_new_dm' => array(
      'label' => t('User sends a direct message'),
      'module' => 'message',
      'group' => 'Message',
      'variables' => array(
        'to' => array('type' => 'list<user>', 'label' => t('List of users that receives the direct message.')),
        'body' => array('type' => 'text', 'label' => t('DM Body')),
        'subject' => array('type' => 'text', 'label' => t('DM Subject')),
      ),
       'arguments' => array(
         'to' => array('type' => 'list<user>', 'label' => t('List of users that receives the direct message.')),
         'body' => array('type' => 'text', 'label' => t('DM Body')),
         'subject' => array('type' => 'text', 'label' => t('DM Subject')),
       ),
    ),
  );
}
